-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2018 at 10:18 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_lgupds`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_masterfile`
--

CREATE TABLE IF NOT EXISTS `tbl_masterfile` (
`id` int(11) NOT NULL,
  `EMP_ID` varchar(100) NOT NULL,
  `surname` text NOT NULL,
  `fname` text NOT NULL,
  `nameext` text NOT NULL,
  `mname` text NOT NULL,
  `dob` date NOT NULL,
  `pob` text NOT NULL,
  `sex` text NOT NULL,
  `civil_status` text NOT NULL,
  `height` text NOT NULL,
  `weight` text NOT NULL,
  `blood_type` text NOT NULL,
  `gsis` text NOT NULL,
  `pagibig` text NOT NULL,
  `philhealth` text NOT NULL,
  `sss` text NOT NULL,
  `tin` text NOT NULL,
  `agency_no` text NOT NULL,
  `citizenship` text NOT NULL,
  `country` text NOT NULL,
  `rhouse_no` text NOT NULL,
  `rstreet` text NOT NULL,
  `rsubdivision` text NOT NULL,
  `rbarangay` text NOT NULL,
  `rcity` text NOT NULL,
  `rprovince` text NOT NULL,
  `rzip_code` text NOT NULL,
  `phouse_no` text NOT NULL,
  `pstreet` text NOT NULL,
  `psubdivision` text NOT NULL,
  `pbarangay` text NOT NULL,
  `pcity` text NOT NULL,
  `pprovince` text NOT NULL,
  `pzip_code` text NOT NULL,
  `telephone` text NOT NULL,
  `mobile` text NOT NULL,
  `email` text NOT NULL,
  `sp_surname` text NOT NULL,
  `sp_firstname` text NOT NULL,
  `sp_nameext` text NOT NULL,
  `sp_mname` text NOT NULL,
  `sp_occ` text NOT NULL,
  `sp_busname` text NOT NULL,
  `sp_busadd` text NOT NULL,
  `sp_telno` text NOT NULL,
  `sp_fsurname` text NOT NULL,
  `sp_ffirstname` text NOT NULL,
  `sp_fextname` text NOT NULL,
  `sp_fmname` text NOT NULL,
  `sp_msurname` text NOT NULL,
  `sp_mfirstname` text NOT NULL,
  `sp_mmname` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
`id` int(15) NOT NULL,
  `name` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `contact` text NOT NULL,
  `position` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `username`, `password`, `contact`, `position`) VALUES
(1, 'Jerome Honrado', 'jeromehonrado', '2bb010060d682fee5ad19d973a9a4d2a', '09057987803', 'Maintainer'),
(3, 'Rustom P. Mamongay', 'rustom', 'a32b9cc2ed189b3baee137a86a2712ad', '09123123123', 'I.T Specialist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_masterfile`
--
ALTER TABLE `tbl_masterfile`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `EMP_ID` (`EMP_ID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_masterfile`
--
ALTER TABLE `tbl_masterfile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
