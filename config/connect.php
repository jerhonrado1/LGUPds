<?php 
session_start();
require_once 'dbconfig.php';

if(isset($_POST["login"])) {

	if(empty($_POST['username']) || empty($_POST['password'])){
	$msg = '<label>All fields are required</label>';

	}else{
		$username = trim($_POST['username']);
		$password = trim($_POST['password']);
		$password = md5($password);

		try 
		{	
		
			$stmt = $db_con->prepare("SELECT * FROM tbl_users WHERE username=:username");
			$stmt->execute(array(":username"=>$username));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$count = $stmt->rowCount();
			
			if($row['password']==$password){
				$_SESSION['id'] = $row['id'];
				$_SESSION['name'] = $row['name'];
				$_SESSION['username'] = $row['username'];
				header('location: admin/dashboard.php');
			}
			else{
				$msg = '<label>email or password does not exist.</label>'; // wrong details 
			}
				
		}
		catch(PDOException $error){
			$msg = $error->getMessage();
		}
	}
}
?>