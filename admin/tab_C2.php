<h6>Step 2</h6>
<section>
    <legend>
        <h1>Personal Data Sheet (Page 2)</h1>
    </legend>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <h3>IV. SERVICE ELIGILITY</h3>
                </div>
            </div>
            
            <div class="col-lg-12" style="font-size: 11px">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td rowspan="2" width="0">27. CAREER SERVICE RA 1080 (BOARD BAR) UNDER SPECIAL LAWS/ CES/ CSEE/ BRGY ELIGILITY/ DRIVERS LICENSE</td>
                            <td rowspan="2" width="0">RATING (If Applicable)</td>
                            <td rowspan="2" width="0">DATE OF EXAMINATION / CONFERMENT</td>
                            <td rowspan="2" width="0">PLACE OF EXAMINATION / CONFERMENT</td>
                            <td colspan="2" width="0">LICENSE (If Applicable)</td>
                            <td rowspan="2" width="0">
                                <button type="button" id="addEligibility" class="btn btn-info">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td width="0">from</td>
                            <td width="0">to</td>
                        </tr>
                    </thead>
                    <tbody class="eligibility">
                        <tr>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>                    

           
        </div>
    </div>
</section>