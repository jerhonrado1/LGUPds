<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="../assets/images/users/default.png" alt="user" />
                <!-- this is blinking heartbit-->
                <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text">
                <h5><?php echo $_SESSION['name'];?></h5>
                
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-settings"></i></a>
                <a href="#" class="" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <a href="../config/logout.php" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
            </div>
        </div>
        <!-- End User profile text-->

         <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">HR Management v1.0</li>

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="dashboard.php">
                        <i class="mdi mdi-chart-timeline"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-book-multiple"></i><span class="hide-menu">201 SALN IPCR Files</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void(0);">Add Record</a></li>
                        <li><a href="javascript:void(0);">Upload 201 File</a></li>
                        <li><a href="javascript:void(0);">Upload IPCR File</a></li>
                        <li><a href="javascript:void(0);">View All Records</a></li>
                    </ul>
                </li>
                <li class="{{ Request::is('pds-records') ? 'active' : null }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-file"></i><span class="hide-menu">Personal Data Sheet</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="PDSCreate.php">Add Record</a></li>
                        <li><a href="PDSUpload.php">Upload PDS File</a></li>
                        <li><a href="PDSView.php">View All Records</a></li>
                    </ul>
                </li>
                
                <li class="{{ Request::is('reports') ? 'active' : null }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-printer"></i><span class="hide-menu">Manage Reports</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="ReportsPrint.php">Print Reports</a></li>
                        <li><a href="ReportsMonthly.php">Monthly Reports</a></li>
                        <li><a href="ReportsExport.php">Export Reports</a></li>
                    </ul>
                </li>

                <li class="nav-small-cap">Credentials</li>
                <li class="{{ Request::is('manage-users') ? 'active' : null }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-account-key"></i><span class="hide-menu">User Management</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="UserAdd.php">Add User</a></li>
                        <li><a href="UserView.php">View All User</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <!-- End Sidebar scroll-->
</aside>