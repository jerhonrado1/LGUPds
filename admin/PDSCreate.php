<?php include('header.php');?>
<?php include('navigation.php'); ?>
<?php include('menu.php');?>


<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Personal Data Sheet</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Personal Data Sheet</li>
                <li class="breadcrumb-item active">Add Record</li>
            </ol>
        </div>
    </div>

   	<div class="container-fluid">
        <!-- form wizard -->
        <div class="col-12">
            <div class="card">
                <div class="card-body wizard-content">
                    <form action="save_pds.php" class="tab-wizard wizard-circle">
                        <?php include('tab_C1.php');?>
                        <?php include('tab_C2.php');?>
                        <?php include('tab_C3.php');?>
                        <?php include('tab_C4.php');?>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include('footer.php');?>
