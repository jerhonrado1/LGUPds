<!-- NAME OF CHILDREN -->
<script type="text/javascript">
$(function() {
$('#add').click(function() {
addnewrow();
});

$('body').delegate('.remove', 'click', function() {
$(this).parent().parent().remove();
});

});


function addnewrow() {
var n = ($('.details tr').length - 0) + 1;
var tr = '<tr>' +
'<td><input type="text" class="form-control a" name="a[]"></td>' +
'<td><input type="text" class="form-control b" name="b[]"></td>' +
'<td><button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button></td>' +
'</tr>';
$('.details').append(tr);
}
</script>


<!-- ELIGIBILITY -->
<script type="text/javascript">
$(function(){
$('#addEligibility').click(function() {
addnewrows();
});

$('body').delegate('.remove', 'click', function() {
$(this).parent().parent().remove();
});

});


function addnewrows() {
var n = ($('.eligibility tr').length - 0) + 1;
var tr = '<tr>' +
'<td><input type="text" class="form-control" name="a[]"></td>' +
'<td><input type="text" class="form-control" name="b[]"></td>' +
'<td><input type="text" class="form-control" name="c[]"></td>' +
'<td><input type="text" class="form-control" name="d[]"></td>' +
'<td><input type="text" class="form-control" name="e[]"></td>' +
'<td><input type="text" class="form-control" name="f[]"></td>' +
'<td><button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button></td>' +
'</tr>';
$('.eligibility').append(tr);
}
</script>


            <footer class="footer"> © 2018 Municipality of Urdaneta City. </footer>
        </div>
    </div>  <!-- Closing main-wrapper -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <script src="../assets/plugins/raphael/raphael-min.js"></script>
    <script src="../assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Chart JS -->
    <script src="js/dashboard1.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

    <!-- WIZARD -->
     <!-- <script src="../assets/plugins/moment/min/moment.min.js"></script> -->
    <script src="../assets/plugins/wizard/jquery.steps.min.js"></script>
    <script src="../assets/plugins/wizard/jquery.validate.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="../assets/plugins/wizard/steps.js"></script>


    <!-- DATATABLES INCLUDE -->
    <!-- Footable -->
    <script src="../assets/plugins/footable/js/footable.all.min.js"></script>
    <script src="../assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

    

    <?php include_once('include-datatable.php');?>

    <!--FooTable init-->
    <script src="js/footable-init.js"></script>
</body>

</html>